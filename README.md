Talk With Terry!
============

This simple program lets you talk with Terry A. Davis.

The default list includes things Terry has said in the past, and those things are very racist.

Therefore, it is recommended not to use the default list if you are uncomfortable.

![Screenshot of me talking with terry](https://imgur.com/kvfdm8K.png)

## Setup

Clone this repo to your desktop and build with `zig build`

---

## Usage

`./twt {LIST}`

Default list is located at src/list.txt

---

## License

>You can check out the full license [here](https://git.ecma.fun/ecma/twt/src/branch/master/LICENSE)

This project is licensed under the terms of the **WTFPL** license.

