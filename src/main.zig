const std = @import("std");
const print = std.debug.print;
const c_alloc = std.heap.c_allocator;

pub fn main() !void {
    var path = argToStr(std.os.argv[1]);

    var text = try readFile(path);
    var word_list = try tokenize(text);
    var bigrams = try pairTwoWords(word_list);

    // Create a 128 word long sentence
    try randomWord(bigrams, 128);
    defer {
        c_alloc.free(text);
        c_alloc.free(bigrams);
        c_alloc.free(word_list);
    }
}

fn argToStr(arg: [*:0]u8) []const u8 {
    var str: []const u8 = undefined;
    var i: u8 = 0;

    // Find the last index of arg
    // Kinda like readUntilDelimiter
    while (true) {
        if (arg[i] == 0) break;
        i += 1;
    }
    // Copy all the chars to the new string
    str = arg[0..i];
    return str;
}

// Self explanatory function
fn readFile(path: []const u8) ![]const u8 {
    var file = try std.fs.cwd().openFile(path, .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();

    return in_stream.readAllAlloc(c_alloc, 4096 * 4);
}

// Split text by a single space
// Deallocate the return value after you are done using it
fn tokenize(text: []const u8) ![][]const u8 {
    var word_list = std.ArrayList([]const u8).init(c_alloc);

    var tokens = std.mem.split(u8, text, " ");
    while (tokens.next()) |gram| {
        try word_list.append(gram);
    }

    return word_list.toOwnedSlice();
}

// Return bigrams of a sentence
// For ex: Bigrams of sentencte "lmao git gud" is { {lmao, git}, {git, gud} }
fn pairTwoWords(word_list: [][]const u8) ![][][]const u8 {
    var bigrams = std.ArrayList([][]const u8).init(c_alloc);

    var i: usize = 0;
    while (i + 1 < word_list.len) {
        try bigrams.append(word_list[i .. i + 2]);
        i += 1;
    }

    return bigrams.toOwnedSlice();
}

// Return a random number between zero and given length
// ziglearn.org/chapter-2/#random-numbers
fn randomIndex(length: usize) !usize {
    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        try std.os.getrandom(std.mem.asBytes(&seed));
        break :blk seed;
    });
    const rand = prng.random();

    return @mod(rand.int(u8), length);
}

// Return a "times" long sentence
fn randomWord(bigrams: [][][]const u8, times: u8) !void {
    var i: u8 = 0;
    var random = try randomIndex(bigrams.len);
    var word: []const u8 = bigrams[random][0]; // Select a random word from bigrams
    print("{s} ", .{word}); // Print the first word

    while (i < times) {
        var word_list = std.ArrayList([]const u8).init(c_alloc);

        // Find the next word based on the current word and add it to an arraylist of strings
        for (bigrams) |bigram| {
            if (std.mem.eql(u8, word, bigram[0])) {
                try word_list.append(bigram[1]);
            }
        }
        // Probably end of sentence
        if (word_list.items.len == 0) {
            break;
        }

        // Randomly choose the next word and print it
        random = try randomIndex(word_list.items.len);
        word = word_list.items[random]; // Store the new word and repeat the process
        print("{s} ", .{word});

        defer word_list.deinit();
        i += 1;
    }
}
